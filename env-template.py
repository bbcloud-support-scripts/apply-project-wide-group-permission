username = "" # Cloud admin username
password = "" # Cloud app password
workspace = "" # Cloud workspace slug/id (as seen in the url)
project_key = ""  # Found in the project settings in the UI
group_name = "" # slug/id of the group as seen in the url
desired_permission = ""  # can be "read", "write", or "admin"