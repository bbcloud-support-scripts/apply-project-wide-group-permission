## Disclaimer
This tool was NOT written by Atlassian developers and is considered a third-party tool. This means that this is also NOT supported by Atlassian. We highly recommend you have your team review the script before running it to ensure you understand the steps and actions taking place, as Atlassian is not responsible for the resulting configuration.

## Purpose
This script locates all repositories within a given workspace/project and adds a given group access based on the desired permission level specified to reduce manual efforts when working with a large number of repositories.

## How to Use
Edit rename/copy the "env-template.py" file to "env.py" (as env.py is in the .gitignore) and fill it out accordingly.

Configure a python virtual environment and install package dependencies with the follow commands:

        python3 -m venv venv
        source venv/Scripts/activate  # If using gitbash on Windows
        source venv/bin/activate      # If on linux/mac
        pip3 install -r requirements.txt

Once the dependencies are satisfied and you have provided your unique details, simply run the script with Python 3.6+ and follow any prompts.

Run script with python via:

        python3 apply_group.py
