from requests import Session


username = ""
password = ""
workspace = ""
project_key = ""  # Found in the project settings in the UI
group_name = ""
desired_permission = ""  # can be "read", "write", or "admin"

session = Session()
session.auth = (username, password)
base_url = "https://api.bitbucket.org"


def group_owner_lookup(page=None):
    r = session.get(f"{base_url}/1.0/groups/{workspace}/{group_name}")
    r_data = r.json()
    if isinstance(r_data, dict):
        r_data = [r_data]
    for group in r_data:
        if group['name'].lower() == group_name.lower() or group['slug'].lower() == group_name.lower():
            owner = group['owner']['uuid']
            confirm = input(f"Matched group found...\n\towner: {owner}\n\tslug: {group['slug']}\n"
                            f"Is this the group that you would like to apply {desired_permission} permissions to for project {project_key}? (y/n): ")
            if confirm.lower() == "y":
                return owner, group['slug']
    # No paging from this endpoint so if we don't match, simply close.
    exit(f'No group was found that matches the provided group name: "{group_name}"')


def get_repo_names(page=None):
    while True:
        params = {'page': page, 'pagelen': 100}
        r = session.get(f'{base_url}/2.0/repositories/{workspace}', params=params)
        r_data = r.json()
        for repo in r_data.get('values'):
            if repo.get('project').get('key') == project_key:
                print(f'Matched repo: "{repo.get("name")}"')
                yield repo.get('name')

        if not r_data.get('next'):
            return

        if page == None:
            page = 1
        page += 1

def write_permissison(repo_name, group_owner, validated_group_slug):
    data = desired_permission
    r = session.put(f'{base_url}/1.0/group-privileges/{workspace}/{repo_name}/{group_owner}/{validated_group_slug}/', data=data)
    if r.status_code == 200:
        return True
    return False

def main():
    group_owner, validated_group_slug = group_owner_lookup()

    for repo_name in get_repo_names():
        if write_permissison(repo_name, group_owner, validated_group_slug):
            print(f'Successfully set the group "{validated_group_slug}" as "{desired_permission}" in {repo_name}\n')


if __name__ == '__main__':
    main()
